import bgImage1 from './bgImage1.png';
import bgVideo1 from './bgVideo1.webm';
import bgVideo2 from './bgVideo2.mp4';
import bubbleFrame from './bubbleFrame.svg';
import mainFrame from './mainFrame.svg';

//Icons
import beer from './icons/beer.svg';
import bike from './icons/bike.svg';
import butterfly from './icons/butterfly.svg';
import camera from './icons/camera.svg';
import cat from './icons/cat.svg';
import cloud from './icons/cloud.svg';
import coffelove from './icons/coffelove.svg';
import cup from './icons/cup.svg';
import dialogbub from './icons/dialogbub.svg';
import diamond from './icons/diamond.svg';
import dot from './icons/dot.svg';
import dot2 from './icons/dot2.svg';
import eye from './icons/eye.svg';
import fire from './icons/fire.svg';
import glass from './icons/glass.svg';
import heart1 from './icons/heart-1.svg';
import heart2 from './icons/heart-2.svg';
import heart from './icons/heart.svg';
import margarita from './icons/margarita.svg';
import moon from './icons/moon.svg';
import moon2 from './icons/moon2.svg';
import mouse from './icons/mouse.svg';
import music from './icons/music.svg';
import pear from './icons/pear.svg';
import potion from './icons/potion.svg';
import ray from './icons/ray.svg';
import ray2 from './icons/ray2.svg';
import skull from './icons/skull.svg';
import skull2 from './icons/skull2.svg';
import speechbub from './icons/speechbub.svg';
import sun from './icons/sun.svg';
import thunder from './icons/thunder.svg';
import ufo from './icons/ufo.svg';
import wine from './icons/wine.svg';

export {
  bgImage1,
  bgVideo1,
  bgVideo2,
  mainFrame,
  bubbleFrame,
  beer,
  bike,
  butterfly,
  camera,
  cat,
  cloud,
  coffelove,
  cup,
  dialogbub,
  diamond,
  dot,
  dot2,
  eye,
  fire,
  glass,
  heart1,
  heart2,
  heart,
  margarita,
  moon,
  moon2,
  mouse,
  music,
  pear,
  potion,
  ray,
  ray2,
  skull,
  skull2,
  speechbub,
  sun,
  thunder,
  ufo,
  wine
};
