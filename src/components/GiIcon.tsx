interface XYpos {
  x: number;
  y: number;
}

interface IconProps {
  pos: XYpos;
  animated: boolean;
  path: string;
}
// function GiIcon({ pos = { x: 0, y: 0 }, animated = '' }): JSX.Element {

function GiIcon({ pos, animated, path }: IconProps): JSX.Element {
  pos = { x: 1, y: 1 };

  return <img src={path} className={animated ? 'animate-bounce' : ''}></img>;
}

export default GiIcon;
