import { ufo } from '../assets';
import GiIcon from './GiIcon';

function GiIconGroup() {
  // creo lista de iconos al azar y seteo de posiciones random a cada uno
  //mapeo de lista de iconos
  return <GiIcon pos={{ x: 1, y: 0 }} animated={true} path={ufo} />;
}

export default GiIconGroup;
