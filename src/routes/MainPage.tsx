import GiButtonGroup from '../components/GiButtonGroup';
import GiIconGroup from '../components/GiIconGroup';

function MainPage() {
  return (
    <>
      <GiIconGroup />
      <div className="bubbleFrame tracking-tight mx-auto flex justify-center">
        <p>
          Hi there! this is <b>Luigi.</b> <br />
          Welcome to <b>my portfolio</b>
        </p>
      </div>
      <GiButtonGroup />
    </>
  );
}

export default MainPage;
