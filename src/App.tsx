import { lazy, Suspense } from 'react';
import { bgVideo1, bgImage1 } from './assets';
import './App.css';
import { Route, BrowserRouter as Router, Routes } from 'react-router-dom';

const MainPage = lazy(() => import('./routes/MainPage'));
const About = lazy(() => import('./routes/About'));
// puedo definir un 'context' variables globales para evitar pasar un dato componente a componente en el arbol

function App() {
  return (
    <>
      {/* <video className='videoTag' autoPlay loop muted>
      <source src={sample} type='video/mp4' />
    </video> */}
      <img src={bgImage1} className="videoTag" />
      <div className="pageFrame">
        <Router>
          <Suspense fallback={<div>Loading...</div>}>
            <Routes>
              <Route path="/" element={<MainPage />} />
              <Route path="/about" element={<About />} />
            </Routes>
          </Suspense>
        </Router>
      </div>
    </>
  );
}

export default App;
