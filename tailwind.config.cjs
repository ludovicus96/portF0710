/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}"
  ],
  theme: {
    fontFamily: {
      firaMono: ['FiraMono-Regular'],
      firaMonoMedium: ['FiraMono-Medium'],
      firaMonoBold: ['FiraMono-Bold']
    },
    extend: {},
  },
  plugins: [],
}
